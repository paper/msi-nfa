#! /usr/bin/env python
'''
 ' @file   wscript
 ' @author Yanbiao Li <lybmath@ucla.edu>
 ' @date   2015-06-03 02:55:11
 ' @brief  
'''

top = '.'
out = 'build'

VERSION='1.0'
APPNAME='msi-nfa'

def options(opt):
    opt.load('tex')
    opt.load('biber')
    opt.add_option_group('paper options')
    opt.add_option('-v', '--view', action='store_true', default=False,
                   help='''view the output file.''')


def configure(conf):
    conf.load('tex')
    conf.load('biber')
    if not conf.env.PDFLATEX:
	conf.fatal('could not find the program pdflatex')

def build(bld):
    bld(features='tex', source='msi-nfa.tex')
    bld.env.PDF_DIR = bld.path.get_bld()
    if bld.options.view:
        bld.add_post_fun(view)

def view(ctx):
    relPath=ctx.env.PDF_DIR.path_from(ctx.path)
    ctx.exec_command('cp %s/%s.pdf %s.pdf' % (relPath, APPNAME, APPNAME))
    ctx.exec_command('open -a skim {}.pdf'.format(APPNAME))

