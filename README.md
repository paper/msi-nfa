============================================================
This is for writing and formating papers.
------------------------------------------------------------
-step1: ./waf configure

This is requried at the first time or you want to change configurations.

if some required tools are missed, install them to avoid configuration errors.

-step2: ./waf build (or directly ./waf)

the final pdf file will be generated at this directory after this process.

you can also run [./waf build -v] or [./waf -v] to open the pdf file afterwards
